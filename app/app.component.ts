import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'pm-app',
    templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
    pageTitle: string = 'Acme Product Management';

    constructor() { }

    ngOnInit() { }

}